package com.example.prayertrackerapp;

import android.content.DialogInterface;
import android.database.Cursor;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.telephony.SignalStrength;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.SimpleCursorAdapter;
import android.widget.Spinner;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        final PrayerDatabaseHandler db = new PrayerDatabaseHandler(this);

        List<String> spinnerArray = new ArrayList<String>();

        spinnerArray.add("Subuh");
        spinnerArray.add("Zuhr");
        spinnerArray.add("Magrib");
        spinnerArray.add("Isha");

        final ArrayAdapter<String> adapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_dropdown_item, spinnerArray);

        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

        final Spinner prayerItems = findViewById(R.id.spnSalaa);

        prayerItems.setAdapter(adapter);

        Button btnAddPrayer = findViewById(R.id.btnAdd);
        Button btnGetPrayers = findViewById(R.id.btnGet);
        final ListView lstVwPrayers = findViewById(R.id.lstPrayers);

        //Adds the prayer record into the database
        btnAddPrayer.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                String selectedPrayer = prayerItems.getSelectedItem().toString();

                Prayer prayer = new Prayer();

                prayer.setPrayerName(selectedPrayer);

                Toast.makeText(getApplicationContext(), "Prayer Added", Toast.LENGTH_LONG).show();

                db.AddPrayer(prayer);
            }
        });

        //Gets all the prayer record from the database and displays it into the List view
        btnGetPrayers.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Cursor prayereCursor = db.GetAllPrayers();

                if(prayereCursor != null && prayereCursor.getCount() > 0){

                    SimpleCursorAdapter prayerAdapter =  GetAllPrayersSimpleCursorAdapter(prayereCursor);

                    lstVwPrayers.setAdapter(prayerAdapter);

                }
                else {

                    Toast.makeText(MainActivity.this, "No prayer records found", Toast.LENGTH_LONG).show();
                }
            }
        });

        //When long press is clicked on a particular row, a model dialog box appears, stating whether you want to delete this particular record or not
        lstVwPrayers.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {

            public boolean onItemLongClick(AdapterView<?> arg0, View v, int index, long id) {

                final long prayerId = id;

                //Creates an alert dialog box
                AlertDialog.Builder alerDialogBuiler = new AlertDialog.Builder(MainActivity.this);

                alerDialogBuiler.setMessage("Are you sure you want to delete this record");

                alerDialogBuiler.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {

                        Prayer prayer = new Prayer();

                        prayer.setId(prayerId);

                        db.DeletePrayer(prayer);

                        //Refresh the grid view once record has been deleted
                        Cursor prayereCursor = db.GetAllPrayers();

                        if(prayereCursor != null && prayereCursor.getCount() > 0){

                            SimpleCursorAdapter prayerAdapter =  GetAllPrayersSimpleCursorAdapter(prayereCursor);

                            lstVwPrayers.setAdapter(prayerAdapter);

                        }

                        Toast.makeText(MainActivity.this, "Record has been deleted", Toast.LENGTH_LONG).show();
                    }
                });

                alerDialogBuiler.setNegativeButton("No", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        finish();
                    }
                });

                return false;
            }
        });
    }

    //converts cursor it to a simple cursor adaptor
    //@param prayersDb: contains all the prayer.
    private SimpleCursorAdapter GetAllPrayersSimpleCursorAdapter(Cursor prayersDb){

        //From
        String[] columns = new String[] { PrayerDatabaseHandler.PRAYER_NAME, PrayerDatabaseHandler.PRAYER_DATE };

        //The XML defines views which data will be bound to
        int[] to = new int[] {R.id.txtPrayerName, R.id.txtPrayerDate};

        SimpleCursorAdapter dataAdapter = new SimpleCursorAdapter(MainActivity.this, R.layout.prayer_info, prayersDb, columns, to, 0);

        return  dataAdapter;
    }
}
