package com.example.prayertrackerapp;

import java.util.Date;

public class Prayer {

    long _id;
    String prayerName;
    Date prayerDate;

    public Prayer() {

    }

    public Prayer(int id, String prayerName, Date prayerDate){
        this._id = id;
        this.prayerName = prayerName;
        this.prayerDate = prayerDate;
    }

    public Prayer(String prayerName, Date prayerDate){

        this.prayerName = prayerName;
        this.prayerDate = prayerDate;
    }

    public long getId()
    {
        return this._id;
    }

    public void setId(long id){
        this._id = id;
    }

    public String getPrayerName(){
        return this.prayerName;
    }

    public void setPrayerName(String prayerName){
        this.prayerName = prayerName;
    }
}
