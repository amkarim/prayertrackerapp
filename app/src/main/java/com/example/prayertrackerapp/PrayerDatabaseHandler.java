package com.example.prayertrackerapp;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

public class PrayerDatabaseHandler extends SQLiteOpenHelper {

    private static final int DATABASE_VERSION = 2;
    private static final String DATABASE_NAME = "PrayerManager";
    private static final String TABLE_PRAYER = "prayers";
    private static final String _Id = "_id";
    public static final String PRAYER_NAME = "PrayerName";
    public static final String PRAYER_DATE = "PrayerDate";

    public PrayerDatabaseHandler(Context context){
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    //Creating table
    public void onCreate(SQLiteDatabase db){

        String CREATE_PRAYERS_TABLE = "CREATE TABLE " + TABLE_PRAYER + "(" + _Id + " INTEGER PRIMARY KEY, " + PRAYER_NAME + " TEXT," + PRAYER_DATE + " DATETIME)";

        db.execSQL(CREATE_PRAYERS_TABLE);
    }

    //Upgrading database
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion){

        //Drop older table if existed
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_PRAYER);

        //Create tables again
        onCreate(db);
    }

    //This function adds prayer to the database table
    public void AddPrayer(Prayer prayer){

        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues values = new ContentValues();

        values.put(PRAYER_NAME, prayer.getPrayerName()); //Prayer name
        values.put(PRAYER_DATE, GetDateTime()); //Prayer date

        //Inserting row
        db.insert(TABLE_PRAYER, null, values);

        db.close(); //closing the database connection
    }

    private String GetDateTime(){

        SimpleDateFormat dateFormat = new SimpleDateFormat("MM-dd-yyyy HH:mm:ss", Locale.getDefault());

        Date date = new Date();

        return dateFormat.format(date);
    }

    //Deletes a prayer record
    public void DeletePrayer(Prayer prayer){
        SQLiteDatabase db = this.getWritableDatabase();

        db.delete(TABLE_PRAYER, _Id + " = ?", new String[] { String.valueOf(prayer.getId())});

        db.close(); //Closing the connection string
    }

    //This function gets all the prayers
    public Cursor GetAllPrayers(){

        SQLiteDatabase db = this.getWritableDatabase();

        Cursor mCursor = db.query(TABLE_PRAYER, new String[] { _Id, PRAYER_NAME, PRAYER_DATE}, null, null, null, null, null);

        if(mCursor != null){
            mCursor.moveToFirst();
        }

        return mCursor;
    }
}